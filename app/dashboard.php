<html lang="es">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<title>InQuery</title>
	<link href="https://fonts.googleapis.com/css?family=Ubuntu:300,300i,400,400i,700" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Poppins" rel="stylesheet">
	<link rel="stylesheet" href="assets/css/app.css">
	
</head>
<body>
    <?php include "assets/elements/header.php" ?>
	<?php include "assets/elements/sidebar.php" ?>

	<section class="content-wrapper">
		<div class="content content-header">
			<div class="row page-titles">
				<div class="col-md-5 col-8 align-self-center">
					<h1 class="page-title">Dashboard</h1>
					<ol class="breadcrumb">
						<li class="breadcrumb-item">Home</li>
					</ol>
				</div>
				<div class="col-md-7 col-4 align-self-center">
				</div>
			</div>
		</div>
		<div class="content">
			<div class="row">
				<div class="col-12 col-md-4">
					<div class="special-card">
						<h2>Formulario #0303456</h2>
						<div class="row">
							<div class="col-6 special-card-text">
								<p>Porcentaje de respuestas</p>
								<sub>Lorem ipsum dolor sit amet</sub>
							</div>
							<div class="col-6 special-card-graphic">
								<img src="assets/images/round.svg" alt="">
							</div>
						</div>
						<div class="special-card-link">
							<a href="#"> <i class="fas fa-external-link-alt"></i> Ver detalles </a>
						</div>
					</div>
				</div>
				<div class="col-12 col-md-4">
					<div class="special-card">
						<h2>Formulario #0303456</h2>
						<div class="row">
							<div class="col-6 special-card-text">
								<p>Porcentaje de respuestas</p>
								<sub>Lorem ipsum dolor sit amet</sub>
							</div>
							<div class="col-6 special-card-graphic">
								<img src="assets/images/round.svg" alt="">
							</div>
						</div>
						<div class="special-card-link">
							<a href="#"> <i class="fas fa-external-link-alt"></i> Ver detalles </a>
						</div>
					</div>
				</div>
				<div class="col-12 col-md-4">
					<div class="special-card">
						<h2>Formulario #0303456</h2>
						<div class="row">
							<div class="col-6 special-card-text">
								<p>Porcentaje de respuestas</p>
								<sub>Lorem ipsum dolor sit amet</sub>
							</div>
							<div class="col-6 special-card-graphic">
								<img src="assets/images/round.svg" alt="">
							</div>
						</div>
						<div class="special-card-link">
							<a href="#"> <i class="fas fa-external-link-alt"></i> Ver detalles </a>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-12">
					<div class="card">
						<div class="card-header bg-default">
							<h5 class="card-title m-0">sakknkjasbdkjas </h5>
						</div>
						<div class="card-body">
							<div class="table-responsive">
								<table class="table table-bordered">
									<thead>
										<th>Titulo 1</th>
										<th>Titulo 2</th>
										<th>Titulo 3</th>
										<th>Titulo 4</th>
										<th>Titulo 5</th>
									</thead>
									<tbody>
										<?php include "assets/elements/table-line.php" ?>
										<?php include "assets/elements/table-line.php" ?>
										<?php include "assets/elements/table-line.php" ?>
										<?php include "assets/elements/table-line.php" ?>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		
	</section>

	<!-- Font awesome -->
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
	<!-- Scripto -->
	<script src="assets/js/app.js"></script>
	<script src="assets/js/bootstrap.min.js"></script>
	<script src="assets/js/popper.min.js"></script>
</body>
</html>

<html lang="es">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<title>InQuery</title>
	<link href="https://fonts.googleapis.com/css?family=Ubuntu:300,300i,400,400i,700" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Poppins" rel="stylesheet">
	<link rel="stylesheet" href="assets/css/app.css">
	
</head>
<body>
    <?php include "assets/elements/header.php" ?>
	<?php include "assets/elements/sidebar.php" ?>

	<section class="content-wrapper">
		<div class="content content-header">
			<div class="row page-titles">
				<div class="col-md-5 col-8 align-self-center">
					<h1 class="page-title">Default</h1>
					<ol class="breadcrumb">
						<li class="breadcrumb-item"><a href="dashboard.php">Home</a></li>
						<li class="breadcrumb-item active">Default</li>
					</ol>
				</div>
				<div class="col-md-7 col-4 align-self-center">
				</div>
			</div>
		</div>
		<div class="content">
            
            <!-- Aqui va el contenido de la página -->

		</div>
		
	</section>

	<!-- Font awesome -->
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
	<!-- Scripto -->
	<script src="assets/js/app.js"></script>
	<script src="assets/js/bootstrap.min.js"></script>
	<script src="assets/js/popper.min.js"></script>
</body>
</html>

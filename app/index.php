<html lang="es">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<title>InQuery</title>
	<link href="https://fonts.googleapis.com/css?family=Ubuntu:300,300i,400,400i,700" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Poppins" rel="stylesheet">
	<link rel="stylesheet" href="assets/css/app.css">
	
</head>
<body>
	
	<section class="login">
		<div class="login-container">
			<div class="login-form">
				<div class="login-logo">
					<img src="assets/images/logo.png" alt="logo">
				</div>
				<h1>Ingresa Aquí</h1>
				<form action="">
					<div class="form-group">
						<label for="Usuario">Nombre Usuario</label>
						<input type="text" placeholder="Ingresa Nombre de Usuario" class="form-control">
					</div>
					<div class="form-group">
						<label for="Usuario">Contraseña</label>
						<input type="text" placeholder="Ingresa tu Contraseña" class="form-control">
					</div>
					<div class="form-group">
						<!--input type="submit" value="Ingresar" class="btn btn-login" -->
						<a href="dashboard.php" class="btn btn-login">Ingresar</a>
					</div>
					<div class="form-group">
						<a href="#">¿Olvidaste tu contraseña?</a><br>
						<a href="#">¿No tienes una cuenta?</a>
					</div>
				</form>
			</div>
		</div>
	</section>

	<!-- Font awesome -->
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
	<!-- Scripto -->
	<script src="assets/js/app.js"></script>
</body>
</html>

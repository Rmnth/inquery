<html lang="es">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>InQuery</title>
        <link href="https://fonts.googleapis.com/css?family=Ubuntu:300,300i,400,400i,700" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Poppins" rel="stylesheet">
        <link rel="stylesheet" href="assets/css/app.css">
    </head>
    <body>
        <?php include "assets/elements/header.php" ?>
        <?php include "assets/elements/sidebar.php" ?>
        <section class="content-wrapper">
            <div class="content content-header">
                <div class="row page-titles">
                    <div class="col-md-5 col-8 align-self-center">
                        <h1 class="page-title">Carga de archivos</h1>
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="dashboard.php">Home</a></li>
                            <li class="breadcrumb-item active">Carga de archivos</li>
                        </ol>
                    </div>
                    <div class="col-md-7 col-4 align-self-center">
                    </div>
                </div>
            </div>
            <div class="content">
                <!-- Aqui va el contenido de la página -->
                <div class="row">
                    <div class="p-3 col-8 col-sm-6">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title">Cargar encuestados</h4>
                                <p class="card-text">
                                    Pre carga aquí el listado de los sujetos a encuestar o testear
                                </p>
                                <button href="#!" class="btn btn-primary">Cargar encuestados</button>                            
                            </div>
                        </div>
                    </div>
                    <div class="p-3 col-8 col-sm-6">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title">Carga encuesta</h4>
                                <p class="card-text">
                                    Carga aquí una nueva encuesta o test para aplicar a tu muestra (ESTE BOTON TIENE FUNCIONALIDADES, HAGALE CLICK :)
                                </p>
                                <button id="opener" href="#!" class="btn btn-primary">Cargar encuestas</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
		</section>
		
		<!-- ESTE DIV REQUIERE FORMATEO PARA ENSANCHARLO -->
		<div id="dialog-message" title="Escriba el encabezado de la encuesta" class="d-flex justify-content-center">
			<textarea class="p-2 title"></textarea>
		</div>


        <!-- Font awesome -->
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
        <!-- Scripto -->
        <script src="assets/js/app.js"></script>
        <script src="assets/js/bootstrap.min.js"></script>
		<script src="assets/js/popper.min.js"></script>
		

		<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
		<link rel="stylesheet" href="/resources/demos/style.css">
		<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
		<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
		<script>

		// esta función abre la caja de dialogo para carga la encuesta
		$( function() {

			$( "#opener" ).on( "click", function() {
			$( "#dialog-message" ).dialog( "open" );
			});
		} );

		$( function() {
			$( "#dialog-message" ).dialog({
			autoOpen: false,
			modal: true,
			buttons: {
				Ingresar: function() {
					// esta función debiera redirigir a la página de carga
				$( this ).dialog( "close" );
				}
			}
			});
		} );




		</script>

    </body>
</html>
﻿Poder hacer cambios en la maqueta

La maqueta la estaba haciendo usando webpack y complementos para css //Es necesario tener instalado node.js

1# Instalar npm en el repo de la maqueta

npm install

//Esto va a crear la carpeta node_modules con los componentes necesarios para lo demas

2# Ejecutar comando de desarrollo para verificar que esté correcto

npm run dev

3# Ejecutar comando de desarrollo para seguir agregando cosas

npm run watch

Esto dejara corriendo la consola para que cualquier cambio que se realice en el css se agregue automaticamente.

La estructura de directorios

    App //Archivos Finales
        - Aqui solo se crean las vistas y su estructura
        Assets //Estos archivos se generan automaticamente

    Resource //Archivos de desarrollo
        - Aqui se desarrolla, Dah
        Css //Css
            Abstract //Css Genericos que deban usar
            Components //Componentes
            Layout //Elementos de la pagina, usualmente reutilizables
            Page // Css designados para cosas especificas de la pagina, traten de usar clases unicas 
            Vendor // weas varias
            App.scss // Archivo donde se listan todos los archivos que se implementan al css final, este es el archivo que se minifica, no se agrega nada de css ahí
            
            



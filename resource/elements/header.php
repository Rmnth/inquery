<header class="header">
    <div class="header-title">
        <span class="header-title-large">InQuery</span>
        <span class="header-title-min">InQ</span>
    </div>
    <div class="header-content">
        <div class="header-content-options">
            <a href="#" title="Opcion Generica 1"><i class="fas fa-cubes"></i></a>
            <a href="#" title="Opcion Generica 2"><i class="fas fa-cubes"></i></a>
            <a href="#" title="Opcion Generica 3"><i class="fas fa-cubes"></i></a>
        </div>
        <a href="#" class="header-content-user">
            <div class="header-content-user-img"><img src="assets/images/user.jpg" alt=""></div>
            <div class="header-content-user-name">Usuario Genérico</div>
        </a>
    </div>
</header>
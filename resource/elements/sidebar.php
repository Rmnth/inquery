<aside class="main-sidebar">
    <section class="sidebar">
        <form class="sidebar-search">
            <label for="">Buscar</label>
            <input type="text" class="form-control" placeholder="Buscar">
            <button class="btn btn-search"><i class="fa fa-search"></i></button>
        </form>
        <hr>
        <div class="sidebar-nav">
            <ul class="sidebar-nav-list">
                <li class="sidebar-nav-item">
                    <a href="dashboard.php" class="sidebar-nav-item-link">
                        <i class="fas fa-home"></i><span>Dashboard</span>
                    </a>
                </li>
                <li class="sidebar-nav-item">
                    <a href="upload.php" class="sidebar-nav-item-link">
                        <i class="fab fa-elementor"></i><span>Carga de Encuestas</span>
                    </a>
                </li>
                <li class="sidebar-nav-item">
                    <a href="#" class="sidebar-nav-item-link">
                        <i class="fab fa-elementor"></i><span>Vista 2</span>
                    </a>
                </li>
                <li class="sidebar-nav-item">
                    <a href="index.php" class="sidebar-nav-item-link">
                        <i class="fas fa-power-off"></i><span>Salir</span>
                    </a>
                </li>
            </ul>
        </div>
    </section>
</aside>